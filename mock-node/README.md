# Cleo Rest API Mock Server

## Installation

First, install [node](http://nodejs.org).

Then, install required npm packages:

	sudo npm install

# Run Server

To run the server:

	node server.js

# Test Server

To test the server:

	jasmine-node /test
	
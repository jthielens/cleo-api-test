var frisby = require('frisby');

/**
 * This set of tests demonstrate the usage of the Cleo REST API to handle connections
 */


frisby.create('List all connections - GET /connections/')
  .get('http://localhost:8080/api/connections/?externalPassword=pass')
  .expectStatus(200)
  .toss();

frisby.create('Create a new connection - POST /connections/')
  .post('http://localhost:8080/api/connections/?password=pass1&externalPassword=pass2')
  .expectStatus(200)
  .toss();


frisby.create('Get one connection - GET /connections/{connectionId}')
  .get('http://localhost:8080/api/connections/34?externalPassword=pass')
  .expectStatus(200)
  .toss();
frisby.create('Update a connection - PUT /connections/{connectionId}')
  .put('http://localhost:8080/api/connections/2134?password=pass1&externalPassword=pass2')
  .expectStatus(200)
  .toss();
frisby.create('Delete a connection - DELETE /connections/{connectionId}')
  .delete('http://localhost:8080/api/connections/3653?externalPassword=pass')
  .expectStatus(200)
  .toss();

var frisby = require('frisby');
var fs     = require('fs');

/**
 * This set of tests demonstrate the usage of the Cleo REST API to handle certificates
 */

// List certificates
frisby.create('List all certificates - GET /certs/')
  .get('http://localhost:8080/api/certs/')
  .expectStatus(200)
  .expectJSONTypes('resources.?', {
    "status":               "valid",
    "version":              "v3",
    "serialNumber":         "8c5664b8d377798b"
  })
  .expectJSONTypes('resources.?', {
    "status":               "valid",
    "version":              "v1",
    "serialNumber":         "1f42285f3c880f8e3c89b384b3ab1f1c"
  })
  .toss();

frisby.create('List all certificates - GET /certs/ notAfter <= 2017-03-17T00:19:27Z')
  .get("http://localhost:8080/api/certs/?filter=notAfter%20le%202017-03-17T00%3A19%3A27Z")
  .expectStatus(200)
  .expectJSONTypes('resources.?', {
    "status":               "valid",
    "version":              "v1",
    "serialNumber":         "1f42285f3c880f8e3c89b384b3ab1f1c"
  })
  .toss();


// Generate a 509 cert
frisby.create('Generate a X.509 - POST /certs/')
  .post('http://localhost:8080/api/certs/',{
    "requestType":"generateCert",
    "alias":"test",
    "dn":"cn=test,c=us",
    "validity":"24 months",
    "alg":"sha1",
    "keyAlg":"rsa",
    "keySize":2048,
    "keyUsage":["digitalSignature"],
    "password":"secret"
  }, {json: true})
  .expectStatus(200)
  .toss();


// Get a certificate by id
frisby.create('Get a certificate by ID - GET /certs/{certId}')
  .get('http://localhost:8080/api/certs/1234', {
    headers: { "Accept": "application/pkix-cert" }
  })
  .expectStatus(200)
  .expectHeader("Content-Type", "application/pkix-cert")
  .expectBodyContains("Cleo test")
  .toss();

// Import a 509 cert
frisby.create('Import x.509 certificate import - POST /certs/')
  .post('http://localhost:8080/api/certs/',{
    "requestType":"importCert",
    "import": "01234345689ABCDEF"
  }, {json: true})
  .expectStatus(200)
  .toss();

// Generate PGP key/cert
frisby.create("Generate a PGP cert/key genTest - POST /certs/")
  .post("http://localhost:8080/api/certs/", {
    "requestType":"genWithPrivateKey",
    encryptionSubkeySize: 1024
  }, {json: true})
  .expectStatus(200)
  .expectJSON({
    message: "PGP key/cert gen not implemented yet"
  })
  .toss();

// Generate CA cert
frisby.create("Generate CA cert gen for pgp/ssh - POST /certs/")
  .post("http://localhost:8080/api/certs/", {
    "requestType":"genWithPublicKey",
    import: "01234345689ABCDEF",
    dn: "cn=test,c=us"
  }, {json: true})
  .expectStatus(200)
  .expectJSON({
    message: "CA cert gen for pgp/ssh not implemented yet"
  })
  .toss();


// Import a pkcs12 cert
frisby.create("Import a pkcs12  cert - POST /certs/")
  .post("http://localhost:8080/api/certs/", {
    "requestType":"importPKCS12",
    import: "01234345689ABCDEF",
    alias: "my_certificate",
    password: "secret_password"
  }, {json: true})
  .expectStatus(200)
  .expectJSON({
    message: "pkcs12 import not implemented yet"
  })
  .toss();

// test the certs POST endpoint using the certs-post.example file we have
frisby.create("Test certs-post.example file")
  .post("http://localhost:8080/api/certs/",
    JSON.parse(fs.readFileSync("../example/certs-post.example", "utf8")),
    {json: true})
  .expectStatus(200)
  .expectJSON({
    alias: "test",
    dn: "cn=test,c=us",
    validity: "24 months",
    alg: "sha1",
    keyAlg: "rsa",
    keySize: 2048,
    keyUsage: ["digitalSignature"],
    password: "secret"
  })
  .toss();

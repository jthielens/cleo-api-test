var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var validator  = require('is-my-json-valid/require');
var _          = require('underscore');
var fs         = require('fs');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var router = express.Router();

router.get('/', function(req, res) {
  res.json({ message: 'Welcome to the Cleo REST API Mock Server' });   
});

router.route('/certs')

  .get(function(req, res) {
    var results = JSON.parse(fs.readFileSync('../example/certs.example', 'utf8'));

    if(req.param("filter")) {
      var filter   = req.param("filter").split(" ");
      var property = filter[0];
      var operator = filter[1];
      var value    = filter[2];

      var certificates     = _filterCertificates(results.resources, property, operator, value);

      results.resources    = certificates;
      results.totalResults = certificates.length;
      results.itemsPerPage = certificates.length;
    }

    res.json(results);
  })

  .post(function(req, res) {
    var requestBody = req.body;

    var validate = validator("../schema/cert.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema: " + JSON.stringify(validate.errors), 400);
      return;
    }

    var requestHandler = requestTypes[requestBody.requestType];
    if(!requestHandler){
      res.send("500: Body matches schema but there's no handler for requestType" + requestBody.requestType, 500);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/certid-get.example', 'utf8'));
    var responseBody = requestHandler(requestBody, example);
    res.status(201).json(responseBody);
  })
;

router.route('/certs/:certId')
  .get(function(req,res) {
    if(req.get("Accept") !== 'application/pkix-cert') {
      res.send("406: Accept header must be 'application/pkix-cert'", 406);
      return;
    }

    var certificate = fs.readFileSync('../example/certificate.der');
    res.writeHead(200, {'Content-Type': 'application/pkix-cert' });
    res.end(certificate, 'binary');
  })

  .delete(function(req, res){
    res.status(204).end();
  })
;

router.route('/connections')
  .get(function(req, res) {

    var results = JSON.parse(fs.readFileSync('../example/connections.example', 'utf8'));
    res.json(results);
  })

  .post(function(req, res) {
    var requestBody = req.body;

    var validate = validator("../schema/connection.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/connectionid-get.example', 'utf8'));
    res.status(201).json(example);
  })
;

router.route('/connections/:connectionId')
  .get(function(req,res) {
    
    var results = JSON.parse(fs.readFileSync('../example/connectionid-get.example', 'utf8'));
    res.json(results);
  })

  .put(function(req, res){
    var requestBody = req.body;

    if(!req.param('externalPassword')){
      res.send("400: externalPassword parameter is missing", 400);
    }
    
    var validate = validator("../schema/connection.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var results = JSON.parse(fs.readFileSync('../example/connectionid-get.example', 'utf8'));
    res.json(results);
  })

  .delete(function(req, res){
    res.status(204).end();
  })
;

router.route('/connections/:connectionId/clone')
  .post(function(req,res) {
    var requestBody = req.body;

    var validate = validator("../schema/connection.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/connectionid-get.example', 'utf8'));
    res.status(201).json(example);
  })
;

router.route('/connections/:connectionId/outgoingFiles')
  .get(function(req, res) {

    var results = JSON.parse(fs.readFileSync('../example/connectionoutgoingfiles.example', 'utf8'));
    res.json(results);
  })

  .post(function(req,res) {
    var requestBody = req.body;

    var validate = validator("../schema/connectionFile.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/connectionoutgoingfileid-get.example', 'utf8'));
    res.status(201).json(example);
  })
;

router.route('/connections/:connectionId/outgoingFiles/:fileId')
  .get(function(req,res) {
    var results = JSON.parse(fs.readFileSync('../example/connectionoutgoingfileid-get.example', 'utf8'));
    res.json(results);
  })

  .put(function(req, res){
    var requestBody = req.body;

    var validate = validator("../schema/connectionFile.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var results = JSON.parse(fs.readFileSync('../example/connectionoutgoingfileid-get.example', 'utf8'));
    res.json(results);
  })

  .delete(function(req, res){
    res.status(204).end();
  })
;

router.route('/connections/:connectionId/outgoingFiles/:fileId/hold')
  .post(function(req,res) {
    var example = JSON.parse(fs.readFileSync('../example/connectionoutgoingfileid-get.example', 'utf8'));
    res.json(example);
  })
;

router.route('/connections/:connectionId/outgoingFiles/:fileId/release')
  .post(function(req,res) {
    var example = JSON.parse(fs.readFileSync('../example/connectionoutgoingfileid-get.example', 'utf8'));
    res.json(example);
  })
;

router.route('/connections/:connectionId/incomingFiles')
  .get(function(req, res) {

    var results = JSON.parse(fs.readFileSync('../example/connectionincomingfiles.example', 'utf8'));
    res.json(results);
  })

  .post(function(req,res) {
    var requestBody = req.body;

    var validate = validator("../schema/connectionFile.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/connectionincomingfileid-get.example', 'utf8'));
    res.status(201).json(example);
  })
;

router.route('/connections/:connectionId/incomingFiles/:fileId')
  .get(function(req,res) {
    var results = JSON.parse(fs.readFileSync('../example/connectionincomingfileid-get.example', 'utf8'));
    res.json(results);
  })

  .put(function(req, res){
    var requestBody = req.body;

    var validate = validator("../schema/connectionFile.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var results = JSON.parse(fs.readFileSync('../example/connectionincomingfileid-get.example', 'utf8'));
    res.json(results);
  })

  .delete(function(req, res){
    res.status(204).end();
  })
;

router.route('/actions')
  .get(function(req, res) {
    var results = JSON.parse(fs.readFileSync('../example/actions.example', 'utf8'));
    res.json(results);
  })

  .post(function(req, res) {
    var requestBody = req.body;

    var validate = validator("../schema/action.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var example = JSON.parse(fs.readFileSync('../example/actionid-get.example', 'utf8'));
    res.status(201).json(example);
  })
;

router.route('/actions/:actionId')
  .get(function(req,res) {
    var results = JSON.parse(fs.readFileSync('../example/actionid-get.example', 'utf8'));
    res.json(results);
  })

  .put(function(req, res){
    var requestBody = req.body;

    var validate = validator("../schema/action.schema");
    if(!validate(requestBody)) {
      res.send("400: Body doesn't match the expected schema " + JSON.stringify(validate.errors), 400);
      return;
    }

    var results = JSON.parse(fs.readFileSync('../example/actionid-get.example', 'utf8'));
    res.json(results);
  })

  .delete(function(req, res){
    res.status(204).end();
  })
;

router.route('/actions/:actionId/run')
  .post(function(req,res) {
    var example = JSON.parse(fs.readFileSync('../example/job.example', 'utf8'));
    res.json(example);
  })
;

router.route('/resourceFolders')
  .get(function(req, res) {
    var results = JSON.parse(fs.readFileSync('../example/resourcefolders.example', 'utf8'));
    res.json(results);
  })
;

router.route('/transfers')
  .get(function(req, res) {

    var results = JSON.parse(fs.readFileSync('../example/transfers.example', 'utf8'));
    res.json(results);
  })
;

router.route('/transfers/:transferId')
  .get(function(req,res) {
    var results = JSON.parse(fs.readFileSync('../example/transferid-get.example', 'utf8'));
    res.json(results);
  })
;

router.route('/transfers/:transferId/resend')
  .post(function(req,res) {
    var example = JSON.parse(fs.readFileSync('../example/job.example', 'utf8'));
    res.json(example);
  })
;

router.route('/transfers/:transferId/rereceive')
  .post(function(req,res) {
    var example = JSON.parse(fs.readFileSync('../example/connectionincomingfileid-get.example', 'utf8'));
    res.json(example);
  })
;

router.route('/events')
  .get(function(req, res) {

    var results = JSON.parse(fs.readFileSync('../example/events.example', 'utf8'));
    res.json(results);
  })
;

router.route('/events/:eventId')
  .get(function(req,res) {
    var results = JSON.parse(fs.readFileSync('../example/eventid-get.example', 'utf8'));
    res.json(results);
  })
;

app.use('/api', router);

var port = process.env.PORT || 8080;
app.listen(port);
console.log('Successfully started the mock server on port ' + port);

// TODO: clean this up
var operators = {
  "eq": function(value1, value2) {
    return value1 === value2;
  },
  "ne": function(value1, value2) {
    return value1 !== value2;
  },
  "co": function(value1, value2) {
    return value1.contains(value2);
  },
  "sw": function(value1, value2) {
    return value1.indexOf(value2) == 0;
  },
  "ew": function(value1, value2) {
    return value1.indexOf(value2, value1.length - value2.length) !== -1;
  },
  "pr": function(value1) {
    return !_.isNull(value1);
  },
  "gt": function(value1, value2) {
    return value1 > value2;
  },
  "ge": function(value1, value2) {
    return value1 >= value2;
  },
  "lt": function(value1, value2) {
    return value1 < value2;
  },
  "le": function(value1, value2) {
    return value1 <= value2;
  }
};

var requestTypes = {
  "generateCert": function(requestBody, responseExample) {
    return _.extend(responseExample, requestBody);
  },
  "importCert": function(requestBody, responseExample) {
    console.log("Simulating x.509 import");
    return responseExample;
  },
  "genWithPrivateKey": function(requestBody, responseExample) {
    return { message: "PGP key/cert gen not implemented yet" };
  },
  "genWithPublicKey": function(requestBody, responseExample) {
    return { message: "CA cert gen for pgp/ssh not implemented yet" };
  },
  "importPKCS12": function(requestBody, responseExample) {
    return { message: "pkcs12 import not implemented yet" };
  }
};

function _filterCertificates(certs, property, operator, value) {
  var validCerts = [];

  for(var i = 0; i < certs.length; ++i) {
    var certificate = certs[i];

    if(operators[operator](certificate[property], value)) {
      validCerts.push(certificate);
    }
  }

  return validCerts;
}

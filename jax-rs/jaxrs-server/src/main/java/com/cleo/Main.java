package com.cleo;

import com.cleo.webservices.api.impl.Connections;
import com.cleo.webservices.api.impl.Connections;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Logger;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ServerProperties;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/api/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.cleo package
        JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
        JacksonJsonProvider prov = new JacksonJsonProvider();
        
        final ResourceConfig rc = new ResourceConfig()
                .registerClasses(Connections.class)
//                .packages("com.cleo.webservices.api.impl;com.cleo.webservices.api.model")
                .register(new UncaughtExceptionMapper())
                .register(new ConnectionValidationReader())
                .register(prov)
                .register(new LoggingFilter(Logger.getLogger(Main.class.getName()), true))
                .property(ServerProperties.TRACING, "ALL");
      
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }
}


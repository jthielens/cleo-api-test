/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo;

import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author rpassmore
 */
@Provider
public class UncaughtExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = Logger.getLogger(UncaughtExceptionMapper.class.getName());

    @Context
    HttpServletRequest request;

    @Override
    public Response toResponse(Throwable exception) {
        System.out.println("throwable: " + exception);

        Throwable badRequestException = getBadRequestException(exception);
        if (badRequestException != null) {
            return Response.status(Status.BAD_REQUEST)
                    .entity(badRequestException.getMessage())
                    .build();
        }
        if (exception instanceof WebApplicationException) {
            return ((WebApplicationException) exception)
                    .getResponse();
        }
        return Response.serverError()
                .entity(exception.getMessage())
                .build();
    }

    private Throwable getBadRequestException(
            Throwable exception) {
        if (exception instanceof ValidationException) {
            return exception;
        }
        Throwable cause = exception.getCause();
        if (cause != null && cause != exception) {
            Throwable result = getBadRequestException(cause);
            if (result != null) {
                return result;
            }
        }
        if (exception instanceof IllegalArgumentException) {
            return exception;
        }
        if (exception instanceof BadRequestException) {
            return exception;
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo;

import com.cleo.webservices.api.model.Connection;
import com.cleo.webservices.api.support.SchemaUtils;
import com.cleo.webservices.api.support.Schemas;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfiguration;
import com.github.fge.jsonschema.core.load.uri.URITranslatorConfiguration;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.Iterator;
import javax.validation.ValidationException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author rpassmore
 */
@Provider
public class ConnectionValidationReader implements MessageBodyReader<Connection> {

    @Override
    public boolean isReadable(Class<?> type, Type type1, Annotation[] antns, MediaType mt) {
        return type == Connection.class;
    }

    @Override
    public Connection readFrom(Class<Connection> type, Type type1, Annotation[] antns, MediaType mt, 
            MultivaluedMap<String, String> mm, InputStream in) throws IOException, WebApplicationException {
        try {
            byte[] inputData = IOUtils.toByteArray(in);
            String errorMessage = SchemaUtils.validate(Schemas.CONNECTION, inputData);
            if(!errorMessage.isEmpty())
                throw new ValidationException("Error validating: Request Json does not match schema\n" + errorMessage);
            
            ObjectMapper objectMapper = new ObjectMapper();
            Connection connection = objectMapper.readValue(inputData, Connection.class);
            return connection;
        } catch (ProcessingException processingException) {
            throw new javax.ws.rs.ProcessingException("Error deserializing connection", processingException);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Async {
    public Async() {}
    
    private Integer timeout;
    private Integer resends;
    private Integer retryDelay;

    /**
     * @return the timeout
     */
    public Integer getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    /**
     * @return the resends
     */
    public Integer getResends() {
        return resends;
    }

    /**
     * @param resends the resends to set
     */
    public void setResends(Integer resends) {
        this.resends = resends;
    }

    /**
     * @return the retryDelay
     */
    public Integer getRetryDelay() {
        return retryDelay;
    }

    /**
     * @param retryDelay the retryDelay to set
     */
    public void setRetryDelay(Integer retryDelay) {
        this.retryDelay = retryDelay;
    }
    
    
    
}

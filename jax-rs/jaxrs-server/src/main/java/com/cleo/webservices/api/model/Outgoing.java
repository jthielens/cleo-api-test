/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Outgoing {
    public Outgoing() {}
    
    private Boolean encrypt;
    private String encryptionAlgorithm;
    private Boolean sign;
    private Boolean compress;
    private Receipt receipt;
    private Storage storage;

    /**
     * @return the encrypt
     */
    public Boolean getEncrypt() {
        return encrypt;
    }

    /**
     * @param encrypt the encrypt to set
     */
    public void setEncrypt(Boolean encrypt) {
        this.encrypt = encrypt;
    }

    /**
     * @return the encryptionAlgorithm
     */
    public String getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    /**
     * @param encryptionAlgorithm the encryptionAlgorithm to set
     */
    public void setEncryptionAlgorithm(String encryptionAlgorithm) {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    /**
     * @return the sign
     */
    public Boolean getSign() {
        return sign;
    }

    /**
     * @param sign the sign to set
     */
    public void setSign(Boolean sign) {
        this.sign = sign;
    }

    /**
     * @return the compress
     */
    public Boolean getCompress() {
        return compress;
    }

    /**
     * @param compress the compress to set
     */
    public void setCompress(Boolean compress) {
        this.compress = compress;
    }

    /**
     * @return the receipt
     */
    public Receipt getReceipt() {
        return receipt;
    }

    /**
     * @param receipt the receipt to set
     */
    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    /**
     * @return the storage
     */
    public Storage getStorage() {
        return storage;
    }

    /**
     * @param storage the storage to set
     */
    public void setStorage(Storage storage) {
        this.storage = storage;
    }

}

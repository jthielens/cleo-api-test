/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class Orchestration {
    public Orchestration() {}
    
    private String emailOnFail;
    private String executeOnFail;
    private String macroDateFormat;
    private String macroTimeFormat;

    /**
     * @return the emailOnFail
     */
    public String getEmailOnFail() {
        return emailOnFail;
    }

    /**
     * @param emailOnFail the emailOnFail to set
     */
    public void setEmailOnFail(String emailOnFail) {
        this.emailOnFail = emailOnFail;
    }

    /**
     * @return the executeOnFail
     */
    public String getExecuteOnFail() {
        return executeOnFail;
    }

    /**
     * @param executeOnFail the executeOnFail to set
     */
    public void setExecuteOnFail(String executeOnFail) {
        this.executeOnFail = executeOnFail;
    }

    /**
     * @return the macroDateFormat
     */
    public String getMacroDateFormat() {
        return macroDateFormat;
    }

    /**
     * @param macroDateFormat the macroDateFormat to set
     */
    public void setMacroDateFormat(String macroDateFormat) {
        this.macroDateFormat = macroDateFormat;
    }

    /**
     * @return the macroTimeFormat
     */
    public String getMacroTimeFormat() {
        return macroTimeFormat;
    }

    /**
     * @param macroTimeFormat the macroTimeFormat to set
     */
    public void setMacroTimeFormat(String macroTimeFormat) {
        this.macroTimeFormat = macroTimeFormat;
    }
    
}

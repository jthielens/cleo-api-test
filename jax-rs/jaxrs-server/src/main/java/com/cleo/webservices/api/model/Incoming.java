/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Incoming {
    
    public Incoming() {}
    
    private Boolean requireEncryption;
    private Boolean requireSignature;
    private Boolean requireReceiptSignature;
    private Storage storage;

    /**
     * @return the requireEncryption
     */
    public Boolean getRequireEncryption() {
        return requireEncryption;
    }

    /**
     * @param requireEncryption the requireEncryption to set
     */
    public void setRequireEncryption(Boolean requireEncryption) {
        this.requireEncryption = requireEncryption;
    }

    /**
     * @return the requireSignature
     */
    public Boolean getRequireSignature() {
        return requireSignature;
    }

    /**
     * @param requireSignature the requireSignature to set
     */
    public void setRequireSignature(Boolean requireSignature) {
        this.requireSignature = requireSignature;
    }

    /**
     * @return the requireReceiptSignature
     */
    public Boolean getRequireReceiptSignature() {
        return requireReceiptSignature;
    }

    /**
     * @param requireReceiptSignature the requireReceiptSignature to set
     */
    public void setRequireReceiptSignature(Boolean requireReceiptSignature) {
        this.requireReceiptSignature = requireReceiptSignature;
    }

    /**
     * @return the storage
     */
    public Storage getStorage() {
        return storage;
    }

    /**
     * @param storage the storage to set
     */
    public void setStorage(Storage storage) {
        this.storage = storage;
    }
    
}

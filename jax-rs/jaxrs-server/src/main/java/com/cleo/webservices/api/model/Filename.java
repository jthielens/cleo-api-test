/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Filename {

    public Filename() {}

    private Boolean useDefaultFilename;
    private String defaultFilename;
    private Boolean overWrite;

    /**
     * @return the useDefaultFilename
     */
    public Boolean getUseDefaultFilename() {
        return useDefaultFilename;
    }

    /**
     * @param useDefaultFilename the useDefaultFilename to set
     */
    public void setUseDefaultFilename(Boolean useDefaultFilename) {
        this.useDefaultFilename = useDefaultFilename;
    }

    /**
     * @return the defaultFilename
     */
    public String getDefaultFilename() {
        return defaultFilename;
    }

    /**
     * @param defaultFilename the defaultFilename to set
     */
    public void setDefaultFilename(String defaultFilename) {
        this.defaultFilename = defaultFilename;
    }

    /**
     * @return the overWrite
     */
    public Boolean getOverWrite() {
        return overWrite;
    }

    /**
     * @param overWrite the overWrite to set
     */
    public void setOverWrite(Boolean overWrite) {
        this.overWrite = overWrite;
    }

}

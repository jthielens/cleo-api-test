/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Storage {
    public Storage() {}
    
    private String outbox;
    private String inbox;
    private Filename filename;
    
    /**
     * @return the outbox
     */
    public String getOutbox() {
        return outbox;
    }

    /**
     * @param outbox the outbox to set
     */
    public void setOutbox(String outbox) {
        this.outbox = outbox;
    }

    /**
     * @return the inbox
     */
    public String getInbox() {
        return inbox;
    }

    /**
     * @param inbox the inbox to set
     */
    public void setInbox(String inbox) {
        this.inbox = inbox;
    }

    /**
     * @return the filename
     */
    public Filename getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(Filename filename) {
        this.filename = filename;
    }
    
    
}

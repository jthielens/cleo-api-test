/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.support;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfiguration;
import com.github.fge.jsonschema.core.load.uri.URITranslatorConfiguration;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author rpassmore
 */
public class SchemaUtils {
    
    private static JsonSchemaFactory schemaFactory = null;
    
    public static JsonSchema buildSchema(Schemas schema) throws ProcessingException {
        buildSchemaFactory();
        return schemaFactory.getJsonSchema(schema.getSchema());
    }
    
    public static String validate(Schemas schemaEnum, byte[] jsonData) throws ProcessingException, IOException {
        JsonSchema schema = buildSchema(schemaEnum);
        
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode requestJson = objectMapper.readValue(jsonData, JsonNode.class);

        ProcessingReport report = schema.validate(requestJson);
        System.out.println(report);

        boolean failed = false;
        StringBuilder errorBuffer = new StringBuilder();
        Iterator<ProcessingMessage> iterator = report.iterator();
        while(iterator.hasNext()) {
            ProcessingMessage processingMessage = iterator.next();
            if(processingMessage.getLogLevel().equals(LogLevel.ERROR)) {
                failed = true;
                errorBuffer.append(processingMessage.getMessage()).append("\n");
            }
        }
        
        return errorBuffer.toString();
    }
    
    private static void buildSchemaFactory() {
        if(schemaFactory == null) {
            File schemaFile = new File("../../schema/");
            URI NAMESPACE = schemaFile.toURI();
            URITranslatorConfiguration translatorCfg
                = URITranslatorConfiguration.newBuilder()
                .setNamespace(NAMESPACE).freeze();
            LoadingConfiguration cfg = LoadingConfiguration.newBuilder()
                .setURITranslatorConfiguration(translatorCfg).freeze();

            schemaFactory = JsonSchemaFactory.newBuilder()
                .setLoadingConfiguration(cfg).freeze();
        }
    }
}

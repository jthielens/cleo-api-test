/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Accept {
    public Accept() {}
    
    private Boolean requireSecurePort;

    /**
     * @return the requireSecurePort
     */
    public Boolean getRequireSecurePort() {
        return requireSecurePort;
    }

    /**
     * @param requireSecurePort the requireSecurePort to set
     */
    public void setRequireSecurePort(Boolean requireSecurePort) {
        this.requireSecurePort = requireSecurePort;
    }
}

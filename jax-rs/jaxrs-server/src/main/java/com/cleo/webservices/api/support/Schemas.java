/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.support;

/**
 *
 * @author rpassmore
 */
public enum Schemas {
    CONNECTION("connection.schema"),
    CERTIFICATE("certificate.schema");
    
    private String schema;
    
    Schemas(String schema) {
        this.schema = schema;
    }
    
    String getSchema() { return schema; }
}

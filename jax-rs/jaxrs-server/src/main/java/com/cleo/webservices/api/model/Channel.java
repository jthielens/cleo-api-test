/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Channel {
    public Channel() {}
    
    private Connect connect;
    private Accept accept;

    /**
     * @return the connect
     */
    public Connect getConnect() {
        return connect;
    }

    /**
     * @param connect the connect to set
     */
    public void setConnect(Connect connect) {
        this.connect = connect;
    }

    /**
     * @return the accept
     */
    public Accept getAccept() {
        return accept;
    }

    /**
     * @param accept the accept to set
     */
    public void setAccept(Accept accept) {
        this.accept = accept;
    }
}

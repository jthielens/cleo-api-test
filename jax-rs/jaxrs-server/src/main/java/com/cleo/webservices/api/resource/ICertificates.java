
package com.cleo.webservices.api.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import com.cleo.webservices.api.model.Collection;


/**
 * The collection of certs
 * 
 */
@Path("certs")
public interface ICertificates {


    /**
     * Get all certs, optionally filtered
     * 
     * @param excludedAttributes
     *     which attributes to exclude e.g. certificate,usages,...
     * @param startIndex
     *      0-based start index in the result set
     * @param count
     *     the maximum number of entries to return
     * @param sortBy
     *     which attribute to sort on e.g. notAfter
     * @param sortOrder
     *     ascending or descending sort order
     * @param attributes
     *     which attributes to include e.g. keyId,externalId,...
     * @param filter
     *      e.g. type eq "..." and (usage eq "..." or usage eq "...") and issuer eq "..." and ....
     */
    @GET
    @Produces({
        "application/json"
    })
    ICertificates.GetCertsResponse getCerts(
        @QueryParam("filter")
        String filter,
        @QueryParam("sortBy")
        String sortBy,
        @QueryParam("sortOrder")
        ICertificates.SortOrder sortOrder,
        @QueryParam("startIndex")
        Long startIndex,
        @QueryParam("count")
        Long count,
        @QueryParam("excludedAttributes")
        String excludedAttributes,
        @QueryParam("attributes")
        String attributes)
        throws Exception
    ;

    /**
     * Create a new cert
     * 
     * @param entity
     *      e.g. {
     *       "requestType":"generateCert",
     *       "alias":"test",
     *       "dn":"cn=test,c=us",
     *       "validity":"24 months",
     *       "alg":"sha1",
     *       "keyAlg":"rsa",
     *       "keySize":2048,
     *       "keyUsage":["digitalSignature"],
     *       "password":"secret"
     *     }
     */
    @POST
    @Consumes("application/json")
    @Produces({
        "application/json"
    })
    ICertificates.PostCertsResponse postCerts(Object entity)
        throws Exception
    ;

    /**
     * 
     * @param excludedAttributes
     *     which attributes to exclude e.g. certificate,usages,...
     * @param certId
     *     
     * @param accept
     *     Allow for customization of the response body.  Default will return JSON with certificate as an attribute, however specifying a different Accept header will alter the response to return the certificate in different formats
     * @param attributes
     *     which attributes to include e.g. keyId,externalId,...
     */
    @GET
    @Path("{certId}")
    @Produces({
        "application/json"
    })
    ICertificates.GetCertsByCertIdResponse getCertsByCertId(
        @PathParam("certId")
        String certId,
        @HeaderParam("Accept")
        @DefaultValue("application/json")
        ICertificates.Accept accept,
        @QueryParam("excludedAttributes")
        String excludedAttributes,
        @QueryParam("attributes")
        String attributes)
        throws Exception
    ;

    /**
     * 
     * @param certId
     *     
     * @param externalPassword
     *     the password used to secure the response e.g. secret
     * @param password
     *     the original password e.g. secret
     */
    @DELETE
    @Path("{certId}")
    ICertificates.DeleteCertsByCertIdResponse deleteCertsByCertId(
        @PathParam("certId")
        String certId,
        @QueryParam("password")
        String password,
        @QueryParam("externalPassword")
        String externalPassword)
        throws Exception
    ;

    public enum Accept {

        application_json,
        application_pkcs8,
        application_pcks10,
        application_pkix_cert,
        application_pkcs7_mime,
        application_pem_file,
        application_pkcs12;

    }

    public class DeleteCertsByCertIdResponse
        extends com.cleo.webservices.api.support.ResponseWrapper
    {


        private DeleteCertsByCertIdResponse(Response delegate) {
            super(delegate);
        }

    }

    public class GetCertsByCertIdResponse
        extends com.cleo.webservices.api.support.ResponseWrapper
    {


        private GetCertsByCertIdResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. {
         *   "id":                   "CFF360F524CB20F1FEAD89006F7F586A285B2D5B",
         *   "alias":                "test",
         *   "trusted":              true,
         *   "status":               "valid",
         *   "signatureAlgorithm":   "sha1WithRSAEncryption",
         *   "version":              "v3",
         *   "serialNumber":         "123456789ABCDEF0",
         *   "issuer":               "CN=test,C=US",
         *   "notBefore":            "2015-01-18T12:34:56Z",
         *   "notAfter":             "2017-01-18T12:34:55Z",
         *   "subject":              "CN=test,C=US",
         *   "publicKeyAlgorithm":   "rsa",
         *   "keySize":              2038,
         *   "keyUsage":             ["digitalSignature"],
         *   "hasPrivateKey":        true,
         *   "certificate":          "MIIDAjCCAmsCEB9CKF88iA+++",
         *   "meta": {
         *     "resourceType": "cert",
         *     "created":      "2015-01-18T12:34:56Z",
         *     "lastModified": "2015-01-18T12:34:56Z",
         *     "location":     "https//api.harmony.cleo.com/api/certs/CFF360F524CB20F1FEAD89006F7F586A285B2D5B",
         *     "version":      "456"
         *   }
         * }
         * 
         * @param entity
         *     {
         *       "id":                   "CFF360F524CB20F1FEAD89006F7F586A285B2D5B",
         *       "alias":                "test",
         *       "trusted":              true,
         *       "status":               "valid",
         *       "signatureAlgorithm":   "sha1WithRSAEncryption",
         *       "version":              "v3",
         *       "serialNumber":         "123456789ABCDEF0",
         *       "issuer":               "CN=test,C=US",
         *       "notBefore":            "2015-01-18T12:34:56Z",
         *       "notAfter":             "2017-01-18T12:34:55Z",
         *       "subject":              "CN=test,C=US",
         *       "publicKeyAlgorithm":   "rsa",
         *       "keySize":              2038,
         *       "keyUsage":             ["digitalSignature"],
         *       "hasPrivateKey":        true,
         *       "certificate":          "MIIDAjCCAmsCEB9CKF88iA+++",
         *       "meta": {
         *         "resourceType": "cert",
         *         "created":      "2015-01-18T12:34:56Z",
         *         "lastModified": "2015-01-18T12:34:56Z",
         *         "location":     "https//api.harmony.cleo.com/api/certs/CFF360F524CB20F1FEAD89006F7F586A285B2D5B",
         *         "version":      "456"
         *       }
         *     }
         */
        public static ICertificates.GetCertsByCertIdResponse jsonOK(Object entity) {
            Response.ResponseBuilder responseBuilder = Response.status(200).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new ICertificates.GetCertsByCertIdResponse(responseBuilder.build());
        }

    }

    public class GetCertsResponse
        extends com.cleo.webservices.api.support.ResponseWrapper
    {


        private GetCertsResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. ../example/certs.example
         * 
         * @param entity
         *     ../example/certs.example
         */
        public static ICertificates.GetCertsResponse jsonOK(Collection entity) {
            Response.ResponseBuilder responseBuilder = Response.status(200).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new ICertificates.GetCertsResponse(responseBuilder.build());
        }

    }

    public class PostCertsResponse
        extends com.cleo.webservices.api.support.ResponseWrapper
    {


        private PostCertsResponse(Response delegate) {
            super(delegate);
        }

        /**
         *  e.g. ../example/certs-item.example
         * 
         * 
         * @param entity
         *     ../example/certs-item.example
         *     
         */
        public static ICertificates.PostCertsResponse jsonCreated(Object entity) {
            Response.ResponseBuilder responseBuilder = Response.status(201).header("Content-Type", "application/json");
            responseBuilder.entity(entity);
            return new ICertificates.PostCertsResponse(responseBuilder.build());
        }

    }

    public enum SortOrder {

        ascending,
        descending;

    }

}

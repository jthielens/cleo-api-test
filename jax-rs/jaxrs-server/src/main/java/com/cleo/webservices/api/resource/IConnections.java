/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.resource;

import com.cleo.webservices.api.model.Connection;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author rpassmore
 */
@Path("connections")
public interface IConnections {
    @PUT
    @Path("{connectionId}")
    @Consumes("application/json")
    @Produces({
        "application/json"
    })
    public Connection putConnectionByConnectionId(
        @PathParam("connectionId")
        String connectionId,
        @QueryParam("password")
        String password,
        @QueryParam("externalPassword")
        String externalPassword, 
        Connection connection) 
        throws Exception;
            
}

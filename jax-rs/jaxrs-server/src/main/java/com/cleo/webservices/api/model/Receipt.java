/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Receipt {
    public Receipt() {}
    
    private String type;
    private String micAlgorithm;
    private Async async;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the micAlgorithm
     */
    public String getMicAlgorithm() {
        return micAlgorithm;
    }

    /**
     * @param micAlgorithm the micAlgorithm to set
     */
    public void setMicAlgorithm(String micAlgorithm) {
        this.micAlgorithm = micAlgorithm;
    }

    /**
     * @return the async
     */
    public Async getAsync() {
        return async;
    }

    /**
     * @param async the async to set
     */
    public void setAsync(Async async) {
        this.async = async;
    }
    
}

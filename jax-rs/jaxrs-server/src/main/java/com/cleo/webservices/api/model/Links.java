/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {
    public Links() {}
    
    private Self self;
    private Predecessor predecessor;

    /**
     * @return the self
     */
    public Self getSelf() {
        return self;
    }

    /**
     * @param self the self to set
     */
    public void setSelf(Self self) {
        this.self = self;
    }

    /**
     * @return the predecessor
     */
    public Predecessor getPredecessor() {
        return predecessor;
    }

    /**
     * @param predecessor the predecessor to set
     */
    public void setPredecessor(Predecessor predecessor) {
        this.predecessor = predecessor;
    }
    
}

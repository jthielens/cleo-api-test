/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cleo.webservices.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rpassmore
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Connection {
    public Connection() {}
    
    private String id;
    private String externalId;
    private String alias;
    private String type;
    private String resourceFolder;
    private Boolean active;
    private Boolean editable;
    private Boolean runnable;
    private Boolean ready;
    private Boolean enabled;
    private String notes;
    private Boolean test;
    private String localName;
    private String partnerName;
    private String subject;
    @JsonProperty("content-type")
    private String contentType;
    private String cemCapable;
    private Channel channel;
    private Outgoing outgoing;
    private Incoming incoming;
    private Orchestration orchestration;
    @JsonProperty("_links")
    private Links _links;
    private Meta meta;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the externalId
     */
    public String getExternalId() {
        return externalId;
    }

    /**
     * @param externalId the externalId to set
     */
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the resourceFolder
     */
    public String getResourceFolder() {
        return resourceFolder;
    }

    /**
     * @param resourceFolder the resourceFolder to set
     */
    public void setResourceFolder(String resourceFolder) {
        this.resourceFolder = resourceFolder;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the editable
     */
    public Boolean getEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    /**
     * @return the runnable
     */
    public Boolean getRunnable() {
        return runnable;
    }

    /**
     * @param runnable the runnable to set
     */
    public void setRunnable(Boolean runnable) {
        this.runnable = runnable;
    }

    /**
     * @return the ready
     */
    public Boolean getReady() {
        return ready;
    }

    /**
     * @param ready the ready to set
     */
    public void setReady(Boolean ready) {
        this.ready = ready;
    }

    /**
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the test
     */
    public Boolean getTest() {
        return test;
    }

    /**
     * @param test the test to set
     */
    public void setTest(Boolean test) {
        this.test = test;
    }

    /**
     * @return the localName
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * @param localName the localName to set
     */
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    /**
     * @return the partnerName
     */
    public String getPartnerName() {
        return partnerName;
    }

    /**
     * @param partnerName the partnerName to set
     */
    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the cemCapable
     */
    public String getCemCapable() {
        return cemCapable;
    }

    /**
     * @param cemCapable the cemCapable to set
     */
    public void setCemCapable(String cemCapable) {
        this.cemCapable = cemCapable;
    }

    /**
     * @return the channel
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    /**
     * @return the outgoing
     */
    public Outgoing getOutgoing() {
        return outgoing;
    }

    /**
     * @param outgoing the outgoing to set
     */
    public void setOutgoing(Outgoing outgoing) {
        this.outgoing = outgoing;
    }

    /**
     * @return the incoming
     */
    public Incoming getIncoming() {
        return incoming;
    }

    /**
     * @param incoming the incoming to set
     */
    public void setIncoming(Incoming incoming) {
        this.incoming = incoming;
    }

    /**
     * @return the orchestration
     */
    public Orchestration getOrchestration() {
        return orchestration;
    }

    /**
     * @param orchestration the orchestration to set
     */
    public void setOrchestration(Orchestration orchestration) {
        this.orchestration = orchestration;
    }

    /**
     * @return the _links
     */
    public Links getLinks() {
        return _links;
    }

    /**
     * @param _links the _links to set
     */
    public void setLinks(Links _links) {
        this._links = _links;
    }

    /**
     * @return the meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * @param meta the meta to set
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }
    
    
    
}

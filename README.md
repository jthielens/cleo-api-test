# Cleo Rest API
This project serves as a specification, documentation, and mock of the Cleo Rest API.

## Installation

First, install [node](http://nodejs.org).

You can use the following tools to help build RAML:

To determine if the RAML is valid, use [raml-cop](https://github.com/thebinarypenguin/raml-cop):
	
	sudo npm i -g raml-cop
	raml-cop api/api.raml

To generate the HTML documentation, use [raml2html](https://github.com/kevinrenskers/raml2html):
	
	sudo npm i -g raml2html
	raml2html api/api.raml > doc/api.html


